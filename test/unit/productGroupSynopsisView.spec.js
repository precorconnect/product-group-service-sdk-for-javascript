import ProductGroupSynopsisView from '../../src/productGroupSynopsisView';
import dummy from '../dummy';

describe('ProductGroupSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ProductGroupSynopsisView(null, dummy.productGroupName);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new ProductGroupSynopsisView(expectedId, dummy.productGroupName);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ProductGroupSynopsisView(dummy.productGroupId, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.productGroupName;

            /*
             act
             */
            const objectUnderTest =
                new ProductGroupSynopsisView(dummy.productGroupId, expectedName);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });

    })
});
