/**
 * @module
 * @description product group service sdk public API
 */
export {default as ProductGroupServiceSdkConfig } from './productGroupServiceSdkConfig'
export {default as ProductGroupSynopsisView} from './productGroupSynopsisView';
export {default as default} from './productGroupServiceSdk';