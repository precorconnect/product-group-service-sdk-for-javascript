## Description
Precor Connect product group service SDK for javascript.

## Features

##### List Product Groups
* [documentation](features/ListProductGroups.feature)

## Setup

**install via jspm**  
```shell
jspm install product-group-service-sdk=bitbucket:precorconnect/product-group-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import ProductGroupServiceSdk,{ProductGroupServiceSdkConfig} from 'product-group-service-sdk'

const productGroupServiceSdkConfig = 
    new ProductGroupServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const productGroupServiceSdk = 
    new ProductGroupServiceSdk(
        productGroupServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```